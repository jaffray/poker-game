package myDeckOfCards;

import java.util.Random;

public class myDeck {
	private myCard theDeck[];
	private int counter;
	private final int total = 52;
	private Random randomNumber;
	
	public myDeck(){
		//name the indexs of face array to seem like face values of cards in a deck
		String face[] = {"Ace","1","2","3","4","5","6","7","8","9","10","Jack","Queen","King"};
		//name the indexs of suits array to seem like suits of cards
		String suit[] = {"Clubs","Diamonds","Hearts","Spades"};
		
		theDeck = new myCard[total];
		counter = 0;
		randomNumber = new Random();
		
		for(int count = 0; count < theDeck.length; count++){
			theDeck[count] = new myCard(face[count % 13], suit[count/13]);
		}//end for loop				
	}//end myDeck constructor
	
	public void shuffle(){
		counter = 0;
		for(int count = 0; count < theDeck.length; count++){
			int swap = randomNumber.nextInt(total);
	
		//swap cards
		myCard temp = theDeck[count];
		theDeck[count] = theDeck[swap];
		theDeck[swap] = temp;
		}//end for loop
	}//end shuffle method
	
	public myCard dealCards(){
		if(counter < theDeck.length){
			return theDeck[counter++];
		}else{
			return null;
		}
	}
}//end class
