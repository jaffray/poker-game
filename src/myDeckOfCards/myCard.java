package myDeckOfCards;

public class myCard {
	//needs suit and face value
	
	private String face;
	private String suit;
	
	public myCard(String face, String suit){
		this.face = face;
		this.suit = suit;
	}
	
	public String toString(){
		return face + " of " + suit;
	}
	
}
