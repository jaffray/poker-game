package myDeckOfCards;

public class myDeckTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//get a deck
		myDeck myDeckOfCards = new myDeck();
		//shuffle the deck
		myDeckOfCards.shuffle();
		//deal the deck
		for(int i = 0; i<13; i++){	
			System.out.printf("%-20s-%-20s-%-20s-%-20s\n", 
					myDeckOfCards.dealCards(), myDeckOfCards.dealCards(),
					myDeckOfCards.dealCards(), myDeckOfCards.dealCards());
		}//end for loop
		
	}//end main

}//end class
